package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"os/exec"
	"path/filepath"
	"runtime"
	"time"
)

func main() {
	_, filePath, _, ok := runtime.Caller(0)
	if !ok {
		log.Fatal("Caller() failed")
	}

	path, port := filepath.Join(filepath.Dir(filePath), "app"), 3000
	args := []string{
		`--appid="cccaaaa1-ed42-11ce-bacd-00aa0057b111"`,
		`--login="demo@example.com"`,
		`--pass="12345"`,
		`--restdebug`,
		fmt.Sprintf(`--port=%d`, port),
	}

	for i := 1; ; i++ {
		log.Println("INFO. >> New iteration:", i)
		cmd := exec.Command(path, args...)
		cmd.Stdout, cmd.Stderr = os.Stdout, os.Stderr
		cmd.Dir = filepath.Dir(filePath)

		log.Println("INFO. Starting...")
		if err := cmd.Start(); err != nil {
			log.Fatal("cmd.Start():", err)
		}

		log.Println("INFO. Started. Waiting some time")
		time.Sleep(3 * time.Second)

		log.Println("INFO. Posting /manager/quit ...")
		resp, err := http.DefaultClient.Post(fmt.Sprintf("http://localhost:%d/api/v2/manager/quit", port),
			"application/json", nil)
		if err != nil {
			log.Println("ERROR. Post():", err)
			log.Println("INFO. Waiting some time before trying again...")
			time.Sleep(3 * time.Second)
			continue
		}
		if resp.StatusCode != http.StatusOK {
			log.Println("ERROR. Killing forcibly.")
			if err := cmd.Process.Kill(); err != nil {
				log.Fatal("cmd.Process.Kill():", err)
			}
		}

		log.Println("INFO. Waiting for process finishes...")
		if _, err := cmd.Process.Wait(); err != nil {
			if _, ok := err.(*exec.ExitError); ok {
				log.Println("INFO. Finish successful. Waiting some time")
			} else {
				log.Fatal("FATAL. cmd.Process.Wait():", err)
			}
		}
		log.Println("INFO. Waiting before restart...")
		time.Sleep(2 * time.Second)
	}

}
